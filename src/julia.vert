#version 330 core
layout (location = 0) in vec3 pos;
uniform vec2 cthing;
out vec4 color;



void normalize_coords(float x, float y, out float xo, out float yo) {
  // Normalized coordinates that go from 0.0 to 1.0
  xo = (x + 1.0) / 2.0;
  yo = (y + 1.0) / 2.0;
}


/* const float CX = -0.74543; */
/* const float CY = 0.21301; */
float CX = cthing.x;
float CY = cthing.y;
// Choose R > 0 such that R^2 - R > sqrt(CX^2 + CY^2)
const float ESCAPE_RADIUS = 4.0;

/* const float MINX = -ESCAPE_RADIUS; */
/* const float MAXX =  ESCAPE_RADIUS; */
/* const float MINY = -ESCAPE_RADIUS; */
/* const float MAXY =  ESCAPE_RADIUS; */

const float ASPECT = 600.0 / 800.0;
const float MINX = -0.2;
const float MAXX = 0.4;
const float MINY = -0.5;
const float MAXY =  MINY + (MAXX - MINX) * ASPECT;


void map_coords(float x, float y, out float xo, out float yo) {
  // Coordinates that go from minx -> maxx and miny -> maxy
  float xn, yn;
  normalize_coords(x, y, xn, yn);

  xo = xn * (MAXX - MINX) + MINX;
  yo = yn * (MAXY - MINY) + MINY;
}

const int MAX_ITERATIONS = 200;

void main() {
  gl_Position = vec4(pos.x, pos.y, pos.z, 1.0);

  float zx, zy;
  map_coords(pos.x, pos.y, zx, zy);

  int iteration = 0;
  while (zx * zx + zy * zy < ESCAPE_RADIUS * ESCAPE_RADIUS && iteration < MAX_ITERATIONS) {
    float xtemp = zx * zx - zy * zy;
    zy = 2 * zx * zy + CY;
    zx = xtemp + CX;

    iteration++;
  }
  
  if (iteration == MAX_ITERATIONS) {
    iteration = 0;
  }

  float gray = pow(float(iteration) / float(MAX_ITERATIONS), 1.0);
  //float angle = atan(zy, zx);
  //color = vec4(gray, gray, angle / (2.0 * 3.14159265358979323), 1.0);
  color = vec4(gray, gray, gray, 1.0);
}
