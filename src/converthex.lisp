



;;(defparameter colors '("#1559ea" "#187fe6" "#1aade3" "#18b7b3" "#13ab87"))
(defparameter colors '(
"#6a96fd"
"#69b1fd"
"#68d3fd"
"#4dfefc"
"#44fed5"))

(defun convert-color (hex-string)
  "Convert hex-string to and RGB representation (list of values 0.0-1.0)."
  (let ((handler (get-handler)))
    (loop for char across hex-string
          do
             (funcall handler char))
    (reverse (funcall handler "state")))
  )

(defun get-handler ()
  (let ((state nil)
        (done nil))
    (lambda (char)
      (cond
        ((equal char "state") state)
        ((and (not done) (>= (length state) 3)) (error (format nil "On input '~A' had already parsed three values: ~A" char state)))
        ((equal char "#") nil)
        ((digit-char-p char)
         (if done
             (progn
               (setf done nil)
               (let ((value (car state)))
                 (setf (car state) (+ (* value 16) (digit-char-p char))))
               )
             (progn
               (push (digit-char-p char) state)
               (setf done t))))
        (t
          (let ((item (find char alphas :key #'first)))
            (when item
              (if done
                  (progn
                    (setf done nil)
                    (let ((value (car state)))
                      (setf (car state) (+ (* value 16) (cdr item))))
                    )
                  (progn
                    (push (cdr item) state)
                    (setf done t)))))))
      state)))

(defparameter alphas
  '((#\a . 10)
    (#\b . 11)
    (#\c . 12)
    (#\d . 13)
    (#\e . 14)
    (#\f . 15)))


(defun convert-alpha (char)
  (let ((item (find char alphas :key #'first)))
    (if item
        (cdr item)
        (error (format nil "Unknown char: ~A" char)))))


(defun write-color (name color)
  "Turn RGB tuple into GLSL declaration."
  (let ((r (/ (first color) 256.0))
        (g (/ (second color) 256.0))
        (b (/ (third color)  256.0)))
    (format t "const vec3 ~a = vec3(~a, ~a, ~a);~%" name r g b)))
  
